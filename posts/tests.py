from django.test import TestCase
from django.urls import reverse
from .models import Post


class PostModelTest(TestCase):
    def setUp(self) -> None:
        Post.objects.create(text="test post")

    def test_text_content(self):
        post = Post.objects.get(id=1)
        test_str = str(post.text)

        self.assertEqual(test_str, "test post")


class HomePageViewTest(TestCase):
    def setUp(self) -> None:
        Post.objects.create(text="test post")

    def test_view_state(self):
        resp = self.client.get('/')
        self.assertEqual(resp.status_code, 200)

    def test_view_url_by_name(self):
        resp = self.client.get(reverse('home'))
        self.assertEqual(resp.status_code, 200)

    def test_correct_template(self):
        resp = self.client.get('/')
        self.assertTemplateUsed(resp, 'home.html')

